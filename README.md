# Fedora-bootstrap-bst

This is a project for unattended and reproducible bootstrap of Fedora, based on
[Freedesktop SDK](https://freedesktop-sdk.io/ ) and [BuildStream](https://gitlab.com/BuildStream/buildstream).

Current status:
* Cross-built a filesystem suitable for running rpmbuild under Qemu
* A single package `xz` is tested.

Next steps:
* Improve the cross-built filesystem that can rebuild all `core` packages and `mock`
* Re-generate the filesystem from just built packages that forms a minimal
  installation of Fedora, which may be dumped into a physical device.
- Mockchain build all packages that can form an Xfce environment.

Further goals:
- Support more aches, like RISC-V, MIPS and PowerPC.
- More tweakable for different generations of x86_64 platform.
