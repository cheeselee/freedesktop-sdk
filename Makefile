SHELL=/bin/bash
BRANCH=fedora-bootstrap-32
ARCH?=$(shell uname -m | sed "s/^i.86$$/i686/" | sed "s/^ppc/powerpc/")
BOOTSTRAP_ARCH?=$(shell uname -m | sed "s/^i.86$$/i686/" | sed "s/^ppc/powerpc/")
ifeq ($(ARCH),i686)
FLATPAK_ARCH=i386
QEMU_ARCH=i386
else ifeq ($(ARCH),powerpc64le)
FLATPAK_ARCH=ppc64le
QEMU_ARCH=ppc64
else
FLATPAK_ARCH=$(ARCH)
QEMU_ARCH=$(ARCH)
endif
REPO=repo
CHECKOUT_ROOT=runtimes
VM_CHECKOUT_ROOT=checkout/$(ARCH)
VM_ARTIFACT_ROOT?=fedora/vm/virt.bst
VM_ARTIFACT_BOOT?=$(VM_ARTIFACT_ROOT)/boot
FAKEROOT?=

ARCH_OPTS=-o bootstrap_build_arch $(BOOTSTRAP_ARCH) -o target_arch $(ARCH)
BST=bst --colors $(ARCH_OPTS)
QEMU=qemu-system-$(QEMU_ARCH)

all: run-vm-build

$(VM_CHECKOUT_ROOT)/$(VM_ARTIFACT_BOOT)/vmlinuz:
	$(BST) build $(VM_ARTIFACT_ROOT)
	$(BST) checkout $(VM_ARTIFACT_ROOT) $(VM_CHECKOUT_ROOT)/$(VM_ARTIFACT_ROOT)

QEMU_COMMON_ARGS= \
	-smp 1 \
	-m 512 \
	-nographic \
	-kernel $(VM_CHECKOUT_ROOT)/$(VM_ARTIFACT_BOOT)/vmlinuz \
	-fsdev local,id=root,path=$(VM_CHECKOUT_ROOT)/$(VM_ARTIFACT_ROOT),security_model=none \
	-device virtio-9p-pci,fsdev=root,mount_tag=/dev/root \

QEMU_X86_COMMON_ARGS= \
	$(QEMU_COMMON_ARGS) \
	$(shell test -f /dev/kvm && echo -enable-kvm) \
	-append 'root=/dev/root rw rootfstype=9p rootflags=trans=virtio,version=9p2000.L,cache=mmap console=ttyS0'

QEMU_ARM_COMMON_ARGS= \
	$(QEMU_COMMON_ARGS) \
	-machine type=virt \
	-cpu max \
	-append 'root=/dev/root rw rootfstype=9p rootflags=trans=virtio,version=9p2000.L,cache=mmap console=ttyAMA0'

QEMU_AARCH64_ARGS= \
	$(QEMU_ARM_COMMON_ARGS)

run-vm: $(VM_CHECKOUT_ROOT)/$(VM_ARTIFACT_BOOT)/vmlinuz
ifeq ($(ARCH),x86_64)
	$(FAKEROOT) $(QEMU) $(QEMU_X86_COMMON_ARGS)
else ifeq ($(ARCH),aarch64)
	$(FAKEROOT) $(QEMU) $(QEMU_AARCH64_ARGS)
endif

run-vm-build: $(VM_CHECKOUT_ROOT)/$(VM_ARTIFACT_BOOT)/vmlinuz
ifeq ($(ARCH),x86_64)
	$(FAKEROOT) $(QEMU) $(QEMU_X86_COMMON_ARGS)' run_build'
else ifeq ($(ARCH),aarch64)
	$(FAKEROOT) $(QEMU) $(QEMU_AARCH64_ARGS)' run_build'
endif
