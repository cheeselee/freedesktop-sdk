#!/bin/bash
export PATH=/usr/bin:/usr/sbin:/bin:/sbin
export USER=root
export HOME=/root
mount -t sysfs none /sys
mount -t proc none /proc
mount -t tmpfs none /tmp
if ! rpm -q redhat-rpm-config; then
    rpm -Uvh --nodeps /noarch-pkgs/*.rpm
    sed -i -e 's/^%_annotated_build/#_annotated_build/' usr/lib/rpm/redhat/macros
fi
mkdir /dev/shm ||:
mount -t tmpfs none /dev/shm
mount -t tmpfs none /run

if grep -q run_build /proc/cmdline; then
    rpmbuild --define '_rpmdir /bin-pkgs' --rebuild --nodeps /src-pkgs/*.rpm
    echo o >/proc/sysrq-trigger
fi
cd $HOME
exec sh -i
